<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>10-Operation</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e59c04ab-acaa-402f-800a-da1653bdfdae</testSuiteGuid>
   <testCaseLink>
      <guid>68a80e63-65b6-4f08-b723-0e44b37a4dd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10-Operation/TCC-01 (Operation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>591f33ae-f7d0-4896-8b9e-3d09d76a2155</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10-Operation/TCC-02 (Client Assignment)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45e9803d-bbd3-496f-aef1-fc783b136258</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10-Operation/TCC-03 (Organization Unit)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dcd8095-9060-485b-a50e-60df55cb6d86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10-Operation/TCC-04 (Assign Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc96329f-59c4-4abf-bedf-b5f86d92dd85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10-Operation/TCC-05 (Unassign Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6372ae0b-f38e-4045-8e55-ec185c3138f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/10-Operation/TCC-08 (Private Circle)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
