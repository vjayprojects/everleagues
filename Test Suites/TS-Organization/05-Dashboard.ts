<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>05-Dashboard</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6b03ba66-97bf-4bfc-8c84-49a7436c0ba6</testSuiteGuid>
   <testCaseLink>
      <guid>36d75fe8-a2f8-4753-9e66-e967d5b39258</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05-Dashboard/TCD-01 (User Login)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>772bd79d-d841-44ff-94d6-99645e6d9aed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05-Dashboard/TCD-02 (Notification Card)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12dc35d5-918a-49dc-a4f6-7b8450e9a64e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05-Dashboard/TCD-03 (Dismiss Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4909160f-2d25-49ed-86d9-023ee8a0ffbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/05-Dashboard/TCD-04 (View Connection)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
