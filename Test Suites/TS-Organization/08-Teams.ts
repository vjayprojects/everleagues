<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>08-Teams</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a0fd745a-a535-4497-98d6-4851cab3b8fa</testSuiteGuid>
   <testCaseLink>
      <guid>b9d3d251-58c2-4448-8d77-97ec2265ede3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-01 (Teams)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4eb8cd59-920c-4eac-aa50-75c761a0373f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-02 (Search Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6aacd521-c019-4bc3-8856-39ed545f577f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-03 (Teams of the Organization)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2dc3208-f099-4346-9fd1-00de8b3a7466</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-04 (Post Box)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea1daa23-ec52-4604-8611-01b924b70c55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-05 (Attachment Icon)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfee7130-c34e-4d70-8a19-95bd378a8822</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-06 (Upload Sample Text)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6c4be1e-d331-45fe-914d-58033b58ba41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-07 (Upload Image)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f83b65f8-d8de-478b-878f-c9181085fdfc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-08 (Files)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84ffee2e-13df-4488-9b11-882c3aaa0c1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-09 (Elipses)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60d8b173-fc75-4e43-93f0-26ef81b58415</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/08-Teams/TCC-10 (Search button-post section)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
