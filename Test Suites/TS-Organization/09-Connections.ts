<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>09-Connections</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>932bfba4-d027-477b-97aa-ddeec3de1efd</testSuiteGuid>
   <testCaseLink>
      <guid>15236bb6-b89f-4b17-8715-bd1f9edbc744</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-01 (Directory)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f823ebbc-437e-4b83-b07b-bd180db80d5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-02 (Connections)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce505338-0a9d-4af4-962c-d60677269dc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-03 (Search Icon)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4f19e98-e353-4f41-b50a-381c0c400c44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-04 (Add New Contact)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fcd9f9d-3b9e-41c6-a839-e978d1eb4450</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-05 (Filter Icon)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5692feba-d533-45f5-9b2a-82515ae45496</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-06 (Contact)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2cf94432-3e07-41ba-95d7-2b31ebcbb269</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-07 (Labels)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5745c22-ca16-46da-8f3d-87a1ea48876b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-08 (Add New Label)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>276c3fa8-dac4-4aa4-b14e-51effc4df202</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-09 (Search Icon)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59e9f916-726c-4bd6-a8b4-f6c81598895b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-10 (Edit Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51bdac97-248f-422a-b84d-8b3682eb0494</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/09-Connections/TCC-11 (Delete Button)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
