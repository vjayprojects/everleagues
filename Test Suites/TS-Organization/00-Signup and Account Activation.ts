<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>00-Signup and Account Activation</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6c04cb97-6fdb-462a-bbf6-74f508759a72</testSuiteGuid>
   <testCaseLink>
      <guid>8710c8d5-729e-4ff8-975d-5eef33e53ce5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00-SignUp and Account Activation/01-Signup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>765f5ad3-a234-4f3d-b574-8f8996cfb298</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00-SignUp and Account Activation/02-Account Activation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80858b10-bbe5-4474-a1d0-c85a5e5774ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00-SignUp and Account Activation/03-Set password</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
