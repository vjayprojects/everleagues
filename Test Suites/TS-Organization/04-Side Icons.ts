<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>04-Side Icons</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>144a67d8-55a1-4ad9-a02d-518443cb744b</testSuiteGuid>
   <testCaseLink>
      <guid>5fe9e574-8944-46a0-a5b7-38d0fcd211bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01-Login/01-Login (valid credentials)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7dafadd0-1750-456e-b2a0-e1f53a5334fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04-Side Icons (Organization)/TCSI-01 (Local Hospital)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c03042b8-8ebe-4ea3-9bcd-130a131d308b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04-Side Icons (Organization)/TCSI-02 (Add Contact)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>995d19d5-585c-4541-8d4e-e1dbe18e4f15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04-Side Icons (Organization)/TCSI-03 (2FA)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb832c6c-90de-4e3a-a1ba-e3a075bebcee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04-Side Icons (Organization)/TCSI-04 (Client Assignment)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1479c61-c7a0-4d09-a35a-0bd6965f6862</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/04-Side Icons (Organization)/TCSI-05 (Hide Quick Shortcuts)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
