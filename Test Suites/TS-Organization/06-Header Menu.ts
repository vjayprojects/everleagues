<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>06-Header Menu</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>81fe0a97-d0bf-449b-baff-ad857c59babb</testSuiteGuid>
   <testCaseLink>
      <guid>b1e1b2bb-b6ca-4b52-8867-282240d53c8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06-Header Menu/TCHM-01 (Users Profile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ef572d5-1b80-4cbe-af75-6cba7a070a9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06-Header Menu/TCHM-02 (View Profile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85e868ea-5d93-4666-8d46-9bfba69dbfbe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06-Header Menu/TCHM-03 (Profile)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>996bed9a-f023-4cb4-a4e7-2887c94c9a1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06-Header Menu/TCHM-04 (Security)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afca2e81-8176-4b54-a12c-41221821cd9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/06-Header Menu/TCHM-05 (Settings)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6b3b29dd-c642-4703-a882-c0eaa86ba14e</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
