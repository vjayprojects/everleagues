<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>11-Settings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5929108c-8b4c-458e-b045-50615b9abe38</testSuiteGuid>
   <testCaseLink>
      <guid>a0cd2683-3f3b-4827-859e-99bd3fee9359</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-01 (Organization)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32c82774-8d77-4fd2-bb97-4d54caf0e46e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-02 (Profile Settings -Changing Organization Image)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b077a30e-da36-4b8b-8182-05b608ead21c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-03 (Address)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bb6e4a4-796c-4605-97ef-3fb09062ec8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-04 (Contact)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13e9fea9-abd6-4f94-8175-063faaea69e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-05 (Save Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4d02359-eb61-4e1b-8ce8-7c302c43d7c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-07 (Payment)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad4be69c-dddf-4ec6-bf01-2863c8faac44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-10 (Add Account Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35f6a7ed-b65f-43a9-8c86-e00735f34d23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-11 (Membership)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2de43bb8-3a3e-45ad-b072-7480b367e694</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-12 (Enable Toggle Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c922ca2a-c9aa-44a1-ba83-f85fce997795</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-13 (Organization Units)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72991fba-cd93-474d-a4e6-e2af2b630400</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-14 (Add Organization Unit Button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>492e6990-781e-42b5-bacf-32f277f8f8b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-15 (Joint Request Link)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77c181aa-af72-47bc-8423-3db541345689</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-16 (Elipses)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcffb108-6adc-43d2-9599-9cf0b48ed019</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-17 (Subscriptions)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2fad2f1-06ad-4580-a1e0-7ec1015a302a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-18 (Actions)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f23d8d80-762f-4477-891f-f531dd0be437</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-19 (Add-Ons)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>513ccf16-e80b-4c29-860a-1461abc44384</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-20 (Team Setup)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae9dfd4b-9a37-4cf7-8f4c-1745b9234778</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-21 (Adding Teams)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f50a058-ee4c-4094-b9e6-2b398cb25dcf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/11-Settings/TCS-22 (Search Icon)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
