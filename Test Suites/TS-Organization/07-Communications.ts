<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>07-Communications</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>fb05daad-e869-4da6-8a0b-d204c5cfa7f2</testSuiteGuid>
   <testCaseLink>
      <guid>91a5cca9-4fff-4f81-97bb-463c55ea586b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07-Communications/TCC-01 (Chat)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b22cc615-5a25-4f42-b546-f5f0a3a5d233</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07-Communications/TCC-02 (Search Icon)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9f21523-53cb-4ce6-bb71-2cbb539630ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07-Communications/TCC-03 (Elipses button)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf0a0e20-1f7f-4c74-9379-dc841d24cb39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07-Communications/TCC-04 (New Chat)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a42d6cf9-1639-4a88-9832-248bd8fc7aeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/07-Communications/TCC-05 (Contact List)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
