import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

WebUI.click(findTestObject('11-Settings/Organization/mat-icon_EditImage'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('11-Settings/Organization/UploadModal'))

CustomKeywords.'uploadpackage.Upload.uploadFile'(findTestObject('11-Settings/Organization/Upload_SelectFile'), 'C:\\Users\\me\\Documents\\everleaguesSample.jpg')

WebUI.delay(5)

WebUI.click(findTestObject('11-Settings/Organization/button_Upload Image'))

WebUI.verifyElementVisible(findTestObject('11-Settings/Organization/img_Profile_large-img'))

WebUI.delay(5)

