<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Client Assignment</name>
   <tag></tag>
   <elementGuidId>54230304-9ddf-4370-af4a-039cb5076fc8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text.ng-tns-c15-30</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/app-root/app-main-page/mat-sidenav-container/mat-sidenav/div/perfect-scrollbar/div/div[1]/px-gdn-sidemenu/ul/px-gdn-sideitem[5]/li/ul/px-gdn-sideitem[1]/li/div/a/span/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text ng-tns-c15-30</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Client Assignment </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-main-page[@class=&quot;ng-tns-c7-3 ng-trigger ng-trigger-routerAnimation ng-star-inserted&quot;]/mat-sidenav-container[@class=&quot;mat-drawer-container mat-sidenav-container mat-drawer-transition mat-drawer-opened&quot;]/mat-sidenav[@class=&quot;left-sidenav mat-drawer mat-sidenav ng-tns-c9-4 ng-trigger ng-trigger-transform ng-tns-c7-3 ng-star-inserted mat-drawer-side&quot;]/div[@class=&quot;mat-drawer-inner-container&quot;]/perfect-scrollbar[@class=&quot;perfect-scrollbar ng-tns-c7-3 ng-star-inserted&quot;]/div[@class=&quot;ps ps--active-y&quot;]/div[@class=&quot;ps-content&quot;]/px-gdn-sidemenu[@class=&quot;ng-tns-c7-3&quot;]/ul[1]/px-gdn-sideitem[@class=&quot;ng-tns-c15-18 ng-tns-c7-3 ng-star-inserted&quot;]/li[@class=&quot;ng-tns-c15-18 menu-level-0 open&quot;]/ul[@class=&quot;children ng-trigger ng-trigger-closeState&quot;]/px-gdn-sideitem[@class=&quot;ng-tns-c15-30 ng-tns-c7-3 ng-star-inserted&quot;]/li[@class=&quot;ng-tns-c15-30 menu-level-1&quot;]/div[@class=&quot;item&quot;]/a[@class=&quot;ng-tns-c15-30 mat-button cdk-focused cdk-mouse-focused&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/div[@class=&quot;text ng-tns-c15-30&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_drop_down'])[2]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Operation'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Private Circle'])[1]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::div[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Client Assignment']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//px-gdn-sideitem[5]/li/ul/px-gdn-sideitem/li/div/a/span/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
