<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_External Contact linkCOPY</name>
   <tag></tag>
   <elementGuidId>9be18ee6-008b-4206-aa72-6d63c52ddf89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>mat-form-field.mat-form-field.ng-tns-c32-80.mat-primary.mat-form-field-type-mat-input.mat-form-field-appearance-outline.mat-form-field-can-float.mat-form-field-should-float.mat-form-field-has-label.ng-untouched.ng-pristine.ng-valid > div.mat-form-field-wrapper > div.mat-form-field-flex</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[5]/div[2]/div/mat-dialog-container/app-dialog-join-req-link/section/mat-dialog-content/form/mat-form-field[2]/div/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-form-field-flex</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>External Contact linkCOPY</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-dialog-0&quot;)/app-dialog-join-req-link[@class=&quot;ng-star-inserted&quot;]/section[1]/mat-dialog-content[@class=&quot;mat-dialog-content&quot;]/form[@class=&quot;ng-pristine ng-valid ng-touched&quot;]/mat-form-field[@class=&quot;mat-form-field ng-tns-c32-80 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-outline mat-form-field-can-float mat-form-field-should-float mat-form-field-has-label ng-untouched ng-pristine ng-valid&quot;]/div[@class=&quot;mat-form-field-wrapper&quot;]/div[@class=&quot;mat-form-field-flex&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//mat-dialog-container[@id='mat-dialog-0']/app-dialog-join-req-link/section/mat-dialog-content/form/mat-form-field[2]/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='COPY'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Client link'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mat-form-field[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
