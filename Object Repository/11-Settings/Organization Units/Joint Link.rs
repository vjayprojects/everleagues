<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Joint Link</name>
   <tag></tag>
   <elementGuidId>c03d875b-25c5-4b80-b2ea-413ef973496a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.mat-icon-button.ng-tns-c31-59.cdk-focused.cdk-mouse-focused > span.mat-button-wrapper > mat-icon.mat-icon.notranslate.ng-tns-c31-59.material-icons.mat-icon-no-color</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/app-root/app-main-page/mat-sidenav-container/mat-sidenav-content/div[1]/app-org-ou/section/article/p-treetable/div/div/table/tbody/tr[2]/td[3]/div/div/table-button-list/div/div/button[2]/span/mat-icon</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>mat-icon</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-icon notranslate ng-tns-c31-59 material-icons mat-icon-no-color</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>link</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-main-page[@class=&quot;ng-tns-c7-3 ng-trigger ng-trigger-routerAnimation ng-star-inserted&quot;]/mat-sidenav-container[@class=&quot;mat-drawer-container mat-sidenav-container mat-drawer-transition mat-drawer-opened&quot;]/mat-sidenav-content[@class=&quot;mat-drawer-content mat-sidenav-content ng-tns-c7-3&quot;]/div[@class=&quot;wrapper ng-tns-c7-3&quot;]/app-org-ou[@class=&quot;ng-tns-c31-59 ng-trigger ng-trigger-routerAnimation ng-star-inserted&quot;]/section[@class=&quot;ng-tns-c31-59&quot;]/article[@class=&quot;ou-container&quot;]/p-treetable[@class=&quot;ng-tns-c31-59&quot;]/div[@class=&quot;ui-treetable ui-widget ui-treetable-hoverable-rows&quot;]/div[@class=&quot;ui-treetable-wrapper ng-star-inserted&quot;]/table[@class=&quot;ui-treetable-table&quot;]/tbody[@class=&quot;ui-treetable-tbody&quot;]/tr[@class=&quot;ng-tns-c31-59 ng-star-inserted&quot;]/td[@class=&quot;ng-tns-c31-59&quot;]/div[@class=&quot;ng-tns-c31-59&quot;]/div[@class=&quot;row-controls&quot;]/table-button-list[@class=&quot;ng-tns-c31-59&quot;]/div[@class=&quot;button-list-container&quot;]/div[@class=&quot;mat-menu-wrapper el-action-menu&quot;]/button[@class=&quot;mat-icon-button ng-tns-c31-59 cdk-focused cdk-mouse-focused&quot;]/span[@class=&quot;mat-button-wrapper&quot;]/mat-icon[@class=&quot;mat-icon notranslate ng-tns-c31-59 material-icons mat-icon-no-color&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Norvs26 Tester'])[3]/following::mat-icon[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Unit'])[1]/following::mat-icon[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EL HealthCheck'])[2]/preceding::mat-icon[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hide Quick Shortcuts'])[1]/preceding::mat-icon[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[2]/td[3]/div/div/table-button-list/div/div/button[2]/span/mat-icon</value>
   </webElementXpaths>
</WebElementEntity>
