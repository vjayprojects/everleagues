<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Toggle</name>
   <tag></tag>
   <elementGuidId>44d7a75b-a57d-471d-8e14-97349484ce4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.mat-slide-toggle-thumb</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/app-root/app-main-page/mat-sidenav-container/mat-sidenav-content/div[1]/app-org-settings/section/article/mat-tab-group/div/mat-tab-body[3]/div/section/div/mat-card/mat-card-content/div[1]/mat-slide-toggle/label/div/div/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-slide-toggle-thumb</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-slide-toggle-1&quot;)/label[@class=&quot;mat-slide-toggle-label&quot;]/div[@class=&quot;mat-slide-toggle-bar mat-slide-toggle-bar-no-side-margin&quot;]/div[@class=&quot;mat-slide-toggle-thumb-container&quot;]/div[@class=&quot;mat-slide-toggle-thumb&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//mat-slide-toggle[@id='mat-slide-toggle-1']/label/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
