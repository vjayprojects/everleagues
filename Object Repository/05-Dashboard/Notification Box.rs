<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Notification Box</name>
   <tag></tag>
   <elementGuidId>32ba2157-761f-4b71-8067-5940dcb71be4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.wrapper.ng-tns-c7-3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/app-root/app-main-page/mat-sidenav-container/mat-sidenav-content/div[1]/app-dashboard-page/section/div/el-card[1]/mat-card</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wrapper ng-tns-c7-3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>User  OneTest OrganizationUser  One has accepted your invitation to join Test Organization.DismissView ConnectionCreate your first organizationEverleaguesStart by creating a new organization and invite users to your organization!DismissCreate OrganizationStill not sure where to start?EverleaguesCheck out our user guides here. If you have any questions, comments, or concerns, please contact us at support@everleagues.com.DismissUser GuideWelcome to Everleagues!EverleaguesWelcome! This is your home screen, where you receive notifications of activity across all your organizations.

The key features of Everleagues are:
Channels: read, write, and comment on posts in your channels
Communications: see all your messages in the current organization
   Home: you are here!
Connections: see all your contacts in the current organization
Tools: use our e-sign tool for tax forms, income-expense tracker, and PDF scanner

In addition to your personal workspace, Everleagues let you connect with one or more organizations you associated with while keeping it easy for you to switch from one organization workspace to another. In the mobile app, click on the organization icon at the top right to switch. You can access the similar function in the web app via the organization dropdown at the top left of the screen.See moreDismiss</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-main-page[@class=&quot;ng-tns-c7-3 ng-trigger ng-trigger-routerAnimation ng-star-inserted&quot;]/mat-sidenav-container[@class=&quot;mat-drawer-container mat-sidenav-container&quot;]/mat-sidenav-content[@class=&quot;mat-drawer-content mat-sidenav-content ng-tns-c7-3&quot;]/div[@class=&quot;wrapper ng-tns-c7-3&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NT'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Norvs26 Tester'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mat-sidenav-content/div</value>
   </webElementXpaths>
</WebElementEntity>
