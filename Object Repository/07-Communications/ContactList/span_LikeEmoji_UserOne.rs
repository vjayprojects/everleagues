<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_LikeEmoji_UserOne</name>
   <tag></tag>
   <elementGuidId>b4c9cc29-8fbb-454d-8bbf-03d985733767</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.emoji-mart-emoji.ng-star-inserted > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/app-root/app-main-page/mat-sidenav-container/mat-sidenav-content/div[1]/app-chat/section/mat-card/mat-card-content/mat-sidenav-container/mat-sidenav-content/div/app-chat-room/div[2]/emoji-mart/div/section/emoji-category[2]/section/ngx-emoji[1]/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-ng-style</name>
      <type>Main</type>
      <value>[object Object]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainContainer&quot;)/app-chat-room[@class=&quot;ng-tns-c37-158 ng-tns-c30-38 ng-star-inserted&quot;]/div[@class=&quot;emoji&quot;]/emoji-mart[@class=&quot;ng-tns-c37-158&quot;]/div[@class=&quot;emoji-mart&quot;]/section[@class=&quot;emoji-mart-scroll&quot;]/emoji-category[@class=&quot;ng-star-inserted&quot;]/section[@class=&quot;emoji-mart-category&quot;]/ngx-emoji[@class=&quot;ng-star-inserted&quot;]/button[@class=&quot;emoji-mart-emoji ng-star-inserted&quot;]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='mainContainer']/app-chat-room/div[2]/emoji-mart/div/section/emoji-category[2]/section/ngx-emoji/button/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='asdgsdag'])[2]/following::span[20]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Thumbs Up Sign'])[1]/preceding::span[1274]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=':+1:'])[1]/preceding::span[1274]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ngx-emoji/button/span</value>
   </webElementXpaths>
</WebElementEntity>
