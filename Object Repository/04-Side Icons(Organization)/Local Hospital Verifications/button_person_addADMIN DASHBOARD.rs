<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_person_addADMIN DASHBOARD</name>
   <tag></tag>
   <elementGuidId>90f65ee6-b2ff-4038-b6b4-73712a068e20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.mat-raised-button.mat-accent.ng-star-inserted.cdk-focused.cdk-mouse-focused</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Active Form(s)'])[1]/preceding::button[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>color</name>
      <type>Main</type>
      <value>accent</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>mattooltip</name>
      <type>Main</type>
      <value>Admin View</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-raised-button mat-accent ng-star-inserted cdk-focused cdk-mouse-focused</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-color</name>
      <type>Main</type>
      <value>accent</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>cdk-describedby-message-8</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-reflect-message</name>
      <type>Main</type>
      <value>Admin View</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>person_add ADMIN DASHBOARD </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-main-page[@class=&quot;ng-tns-c7-3 ng-trigger ng-trigger-routerAnimation ng-star-inserted&quot;]/mat-sidenav-container[@class=&quot;mat-drawer-container mat-sidenav-container mat-drawer-transition mat-drawer-opened&quot;]/mat-sidenav-content[@class=&quot;mat-drawer-content mat-sidenav-content ng-tns-c7-3&quot;]/div[@class=&quot;wrapper ng-tns-c7-3&quot;]/app-infotracker[@class=&quot;ng-star-inserted&quot;]/table[@class=&quot;trackertable5&quot;]/tbody[1]/tr[1]/td[@class=&quot;ng-star-inserted&quot;]/button[@class=&quot;mat-raised-button mat-accent ng-star-inserted cdk-focused cdk-mouse-focused&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active Form(s)'])[1]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/button</value>
   </webElementXpaths>
</WebElementEntity>
