<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Test(posts)</name>
   <tag></tag>
   <elementGuidId>841a2820-5247-46a4-95da-738b78f82279</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ng-tns-c21-40.search-results--item.selected.ng-star-inserted > div.ng-tns-c21-40.ng-star-inserted</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/app-root/app-main-page/mat-sidenav-container/mat-sidenav-content/div[1]/app-teams/section/div[3]/div/perfect-scrollbar/div/div[1]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ng-tns-c21-40 ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-main-page[@class=&quot;ng-tns-c2-0 ng-trigger ng-trigger-routerAnimation ng-star-inserted&quot;]/mat-sidenav-container[@class=&quot;mat-drawer-container mat-sidenav-container mat-drawer-transition&quot;]/mat-sidenav-content[@class=&quot;mat-drawer-content mat-sidenav-content ng-tns-c2-0&quot;]/div[@class=&quot;wrapper ng-tns-c2-0&quot;]/app-teams[@class=&quot;ng-tns-c21-40 ng-star-inserted&quot;]/section[@class=&quot;teams-section&quot;]/div[@class=&quot;teams-search ng-trigger ng-trigger-searchState open&quot;]/div[@class=&quot;search-results&quot;]/perfect-scrollbar[@class=&quot;teams-search-scrollbar teams-search-scrollbar--no-loadmore&quot;]/div[@class=&quot;ps&quot;]/div[@class=&quot;ps-content&quot;]/div[@class=&quot;ng-tns-c21-40 search-results--item selected ng-star-inserted&quot;]/div[@class=&quot;ng-tns-c21-40 ng-star-inserted&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Results'])[1]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comment'])[2]/following::div[19]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EL HealthCheck'])[2]/preceding::div[20]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/perfect-scrollbar/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
